package edu.neumont.socialfitswimmers;

import java.util.ArrayList;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import edu.neumont.Models.Suggestions;
import edu.neumont.Models.User;
import edu.neumont.Models.UserLists;

public class SignUpActivity extends ActionBarActivity 
{
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up);
		
		Parse.initialize(this, "KUOeDfzwtxhQ2ux0zGVgLnxO1XGtpNCS2YFIo065", "sCD4VCKI2J6BpyElGNGH89agLXziONgVh4odYqFU");
	}
	
	public void createAccount(View v)
	{
		EditText first = (EditText) findViewById(R.id.first_name_sign_up_input);
		EditText last = (EditText) findViewById(R.id.last_name_sign_up_input);
		EditText user = (EditText) findViewById(R.id.username_sign_up_input);
		EditText pass = (EditText) findViewById(R.id.password_sign_up_input);
		RadioGroup group = (RadioGroup) findViewById(R.id.account_create_id_radio_group);
		int selectedRadio = group.getCheckedRadioButtonId();
		RadioButton button = (RadioButton) findViewById(selectedRadio);
		
		String firstName = first.getText().toString();
		String lastName = last.getText().toString();
		String username = user.getText().toString();
		String password = pass.getText().toString();
		String accountType = button.getText().toString();
		
		ParseUser newUser = new ParseUser();
		newUser.setUsername(username);
		newUser.setPassword(password);
		newUser.put(User.FIRST_NAME, firstName);
		newUser.put(User.LAST_NAME, lastName);
		newUser.put(User.ACCOUNT_TYPE, accountType);
		newUser.put(User.DESCRIPTION, "");
		newUser.put(User.PROFILE_IMAGE_URL, "");
		newUser.put(User.ACHIEVEMENTS, new ArrayList<String>());
		
		if(accountType.equals(User.ACCOUNT_TYPE_COACH))
		{
			newUser.put(User.COACH_QUALIFICATIONS, new ArrayList<String>());
		}
		
		ParseObject userLists = new ParseObject(UserLists.TABLE_NAME);
		userLists.put(UserLists.USERNAME, username);
		userLists.saveInBackground();
		
		ParseObject suggestions = new ParseObject(Suggestions.TABLE_NAME);
		suggestions.put(Suggestions.USERNAME, username);
		suggestions.saveInBackground();
		
		newUser.signUpInBackground(new SignUpCallback()
		{	
			@Override
			public void done(ParseException e) 
			{
				if(e == null)
				{
					Intent intent = new Intent(getBaseContext(), LogInActivity.class);
					startActivity(intent);
				}
				else
				{
					// Sign up didn't succeed.  Look at the ParseException
					// to figure out what went wrong
					Toast.makeText(getBaseContext(), 
					"Woops something went wrong when signing up!", Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.sign_up, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}