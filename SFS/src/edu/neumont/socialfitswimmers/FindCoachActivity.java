package edu.neumont.socialfitswimmers;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import edu.neumont.Models.User;

public class FindCoachActivity extends ActionBarActivity {
	
	public final static String EXTRA_COACH_USERNAME = "edu.neumont.socialfitswimmers.COACH_USERNAME";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_find_coach);
		
		Parse.initialize(this, "KUOeDfzwtxhQ2ux0zGVgLnxO1XGtpNCS2YFIo065", "sCD4VCKI2J6BpyElGNGH89agLXziONgVh4odYqFU");
		findCoaches();
	}
	
	public void findCoaches()
	{
		ParseQuery<ParseUser> query = ParseUser.getQuery();
		query.whereEqualTo(User.ACCOUNT_TYPE, User.ACCOUNT_TYPE_COACH);
		query.whereNotEqualTo(User.USERNAME, ParseUser.getCurrentUser().getUsername());
		
		List<ParseUser> coachObjects = null;
		try {
			coachObjects = query.find();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<String> coaches = new ArrayList<String>();
		
		for(ParseUser o : coachObjects)
		{
			coaches.add(o.getString(User.USERNAME));
		}
		
		ListAdapter techAdapt = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, coaches);
        ListView coachSelection = (ListView) findViewById(R.id.found_coaches_list);
        coachSelection.setAdapter(techAdapt);
        
        coachSelection.setOnItemClickListener(new AdapterView.OnItemClickListener() 
        {
        	@Override
			public void onItemClick(AdapterView<?> adapterView,	View view, int i, long l) 
        	{   
        		String coachUsernamePicked = adapterView.getItemAtPosition(i).toString();
        		
        		Intent intent = new Intent(getBaseContext(), ProfileActivity.class);
        		intent.putExtra("findClass", "FindCoachActivity");
        		intent.putExtra(EXTRA_COACH_USERNAME, coachUsernamePicked);
        		startActivity(intent);
        	} 
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.find_coach, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}