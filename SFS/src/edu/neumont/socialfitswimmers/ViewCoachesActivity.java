package edu.neumont.socialfitswimmers;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import edu.neumont.Models.UserLists;

public class ViewCoachesActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_coaches);

		Parse.initialize(this, "KUOeDfzwtxhQ2ux0zGVgLnxO1XGtpNCS2YFIo065", "sCD4VCKI2J6BpyElGNGH89agLXziONgVh4odYqFU");
		
		viewCoaches();
	}
	
	public void viewCoaches()
	{
		ParseUser currentUser = ParseUser.getCurrentUser();
		ParseQuery<ParseObject> coachesQuery = ParseQuery.getQuery(UserLists.TABLE_NAME);
		coachesQuery.whereEqualTo(UserLists.USERNAME, currentUser.getUsername());
		ParseObject object = null;
		try {
			object = coachesQuery.getFirst();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<String> list = object.getList(UserLists.MY_COACH_LIST);
		
		if(list != null)
		{
			ListAdapter techAdapt = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, list);
	        ListView coachSelection = (ListView) findViewById(R.id.your_coaches_list);
	        coachSelection.setAdapter(techAdapt);
	        
	        coachSelection.setOnItemClickListener(new AdapterView.OnItemClickListener() 
	        {
	        	@Override
				public void onItemClick(AdapterView<?> adapterView,	View view, int i, long l) 
	        	{   
	        		String coachUsernamePicked = adapterView.getItemAtPosition(i).toString();
	        		
	        		Intent intent = new Intent(getBaseContext(), ProfileActivity.class);
	//        		intent.putExtra("findClass", "FindCoachActivity");
	        		intent.putExtra("viewUserPicked", coachUsernamePicked);
	        		startActivity(intent);
	        	} 
			});
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_coaches, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}