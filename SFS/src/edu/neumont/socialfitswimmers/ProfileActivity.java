package edu.neumont.socialfitswimmers;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import edu.neumont.Models.User;
import edu.neumont.Models.UserLists;

public class ProfileActivity extends ActionBarActivity {
	
	private ImageView view;
	private String usernamePicked;
	private ParseUser currentUser = ParseUser.getCurrentUser();
	private int clickCount = 10;
	private Handler handler = new Handler();
	private static int count;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);
		Parse.initialize(this, "KUOeDfzwtxhQ2ux0zGVgLnxO1XGtpNCS2YFIo065", "sCD4VCKI2J6BpyElGNGH89agLXziONgVh4odYqFU");

		count = 0;
		findViewById(R.id.profile_edit_picture).setVisibility(View.GONE);
		findViewById(R.id.profile_edit_description).setVisibility(View.GONE);
		findViewById(R.id.profile_edit_achievements).setVisibility(View.GONE);
		findViewById(R.id.profile_edit_qualifications).setVisibility(View.GONE);
		
		view = (ImageView) findViewById(R.id.profile_picture);
		final Intent intent = this.getIntent();
		
		if(intent != null)
		{
			String foundClass = intent.getStringExtra("findClass");
			usernamePicked = intent.getStringExtra("viewUserPicked");
			
			if(foundClass != null && foundClass.equals("FindCoachActivity"))
			{
				usernamePicked = intent.getStringExtra(FindCoachActivity.EXTRA_COACH_USERNAME);
				
				findViewById(R.id.profile_add_friend_button).setVisibility(View.GONE);
				
				TextView profileName = (TextView) findViewById(R.id.profile_name);
				profileName.setText(usernamePicked);
			}
			else if(foundClass != null && foundClass.equals("FindFriendActivity"))
			{
				handler.post(new Runnable(){
					@Override
					public void run()
					{
						usernamePicked = intent.getStringExtra(FindFriendActivity.EXTRA_FRIEND_USERNAME);
						TextView profileName = (TextView) findViewById(R.id.profile_name);
						profileName.setText(usernamePicked);
						
						ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
						userQuery.whereEqualTo(User.USERNAME, usernamePicked);
						
						ParseUser user = null;
						try {
							user = userQuery.getFirst();
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						if(user.getString(User.ACCOUNT_TYPE).equals(User.ACCOUNT_TYPE_SWIMMER))
						{
							findViewById(R.id.profile_add_coach_button).setVisibility(View.GONE);
						}
					}
				});
			}
			else if(usernamePicked != null)
			{				
				handler.post(new Runnable()
				{
					@Override
					public void run()
					{
						ParseQuery<ParseUser> query = ParseUser.getQuery();
						query.whereEqualTo(User.USERNAME, usernamePicked);
						
						ParseUser found = null;
						try {
							found = query.getFirst();
						} catch (ParseException e) {
							e.printStackTrace();
						}
						
						if(found.getString(User.ACCOUNT_TYPE).equals(User.ACCOUNT_TYPE_SWIMMER))
						{
							findViewById(R.id.profile_add_coach_button).setVisibility(View.GONE);
							findViewById(R.id.profile_qualifications).setVisibility(View.GONE);
							findViewById(R.id.profile_qualifications_tag).setVisibility(View.GONE);
							findViewById(R.id.profile_swimmers).setVisibility(View.GONE);
							findViewById(R.id.profile_swimmers_tag).setVisibility(View.GONE);
							
							int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 225, getResources().getDisplayMetrics());
							ListView view = (ListView) findViewById(R.id.profile_achievements);
							LayoutParams friendsParams = view.getLayoutParams();
							friendsParams.height = height;
							
							fillCoachesList(usernamePicked);
						}
						else
						{
							findViewById(R.id.profile_coaches).setVisibility(View.GONE);
							findViewById(R.id.profile_coaches_tag).setVisibility(View.GONE);
							fillSwimmersList(usernamePicked);
							fillQualificationsList(usernamePicked);
						}
						
						if(found.getUsername().equals(currentUser.getUsername()))
						{
							findViewById(R.id.profile_add_friend_button).setVisibility(View.GONE);
							findViewById(R.id.profile_add_coach_button).setVisibility(View.GONE);
						}
						
						TextView profileName = (TextView) findViewById(R.id.profile_name);
						profileName.setText(usernamePicked);
					}
				});
			}
		}
		handler.post(new Runnable()
		{
			@Override
			public void run()
			{
				ParseQuery<ParseUser> pickedUserQuery = ParseUser.getQuery();
				pickedUserQuery.whereEqualTo(User.USERNAME, usernamePicked);
				
				ParseUser pickedUser = null;
				try
				{
					pickedUser = pickedUserQuery.getFirst();
				}
				catch (ParseException e1)
				{
					e1.printStackTrace();
				}
				if(pickedUser.getString(User.PROFILE_IMAGE_URL) != null && !pickedUser.getString(User.PROFILE_IMAGE_URL).equals(""))
				{
					new SetProfileImage().execute(pickedUser.getString(User.PROFILE_IMAGE_URL));
				}
				else
				{
					new SetProfileImage().execute("https://www.stereodose.com/Stereodose/static/default_profile_image.jpg");
				}
				
				fillFriendsList(usernamePicked);
				fillAchievementsList(usernamePicked);
				
				TextView description = (TextView) findViewById(R.id.profile_description);
				description.setMovementMethod(new ScrollingMovementMethod());
				
				ParseUser currentUser = ParseUser.getCurrentUser();
				ParseQuery<ParseObject> coachesQuery = ParseQuery.getQuery(UserLists.TABLE_NAME);
				coachesQuery.whereEqualTo(UserLists.USERNAME, currentUser.getUsername());
				ParseObject object = null;
				try {
					object = coachesQuery.getFirst();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				List<String> coachList = object.getList(UserLists.MY_COACH_LIST);
				List<String> friendList = object.getList(UserLists.MY_FRIENDS_LIST);
				
				if(coachList != null)
				{			
					if(coachList.contains(usernamePicked))
					{
						Button b = (Button) findViewById(R.id.profile_add_coach_button);
						b.setVisibility(View.GONE);
					}
				}
				if(friendList != null)
				{			
					if(friendList.contains(usernamePicked))
					{
						Button b = (Button) findViewById(R.id.profile_add_friend_button);
						b.setVisibility(View.GONE);
					}
				}
			}
		});
	}
	
	private void fillFriendsList(String user)
	{
		ParseQuery<ParseObject> friendsQuery = ParseQuery.getQuery(UserLists.TABLE_NAME);
		friendsQuery.whereEqualTo(UserLists.USERNAME, user);
		ParseObject object = null;
		try {
			object = friendsQuery.getFirst();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<String> friends = object.getList(UserLists.MY_FRIENDS_LIST);
		
		if(friends != null)
		{			
			ListAdapter techAdapt = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, friends);
	        ListView coachSelection = (ListView) findViewById(R.id.profile_friends);
	        coachSelection.setAdapter(techAdapt);
	        
	        coachSelection.setOnItemClickListener(new AdapterView.OnItemClickListener() 
	        {
	        	@Override
				public void onItemClick(AdapterView<?> adapterView,	View view, int i, long l) 
	        	{   
	        		String usernamePicked = adapterView.getItemAtPosition(i).toString();
	        		
	        		Intent intent = new Intent(getBaseContext(), ProfileActivity.class);
	        		intent.putExtra("viewUserPicked", usernamePicked);
	        		startActivity(intent);
	        	}
			});
		}
	}
	
	private void fillAchievementsList(String user)
	{
		ParseQuery<ParseUser> friendsQuery = ParseUser.getQuery();
		friendsQuery.whereEqualTo(User.USERNAME, user);
		ParseObject object = null;
		try {
			object = friendsQuery.getFirst();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		List<String> achievements = object.getList(User.ACHIEVEMENTS);
		
		if(achievements != null)
		{			
			ListAdapter techAdapt = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, achievements);
	        ListView coachSelection = (ListView) findViewById(R.id.profile_achievements);
	        coachSelection.setAdapter(techAdapt);
		}
	}
	
	private void fillQualificationsList(String user)
	{
		ParseQuery<ParseUser> friendsQuery = ParseUser.getQuery();
		friendsQuery.whereEqualTo(User.USERNAME, user);
		ParseObject object = null;
		try {
			object = friendsQuery.getFirst();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<String> quals = object.getList(User.COACH_QUALIFICATIONS);
		
		if(quals != null)
		{			
			ListAdapter techAdapt = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, quals);
	        ListView coachSelection = (ListView) findViewById(R.id.profile_qualifications);
	        coachSelection.setAdapter(techAdapt);
		}
	}

	private void fillSwimmersList(String user)
	{
		ParseQuery<ParseObject> friendsQuery = ParseQuery.getQuery(UserLists.TABLE_NAME);
		friendsQuery.whereEqualTo(UserLists.USERNAME, user);
		ParseObject object = null;
		try {
			object = friendsQuery.getFirst();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<String> quals = object.getList(UserLists.MY_SWIMMERS_LIST);
		
		if(quals != null)
		{			
			ListAdapter techAdapt = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, quals);
	        ListView coachSelection = (ListView) findViewById(R.id.profile_swimmers);
	        coachSelection.setAdapter(techAdapt);
	        
	        coachSelection.setOnItemClickListener(new AdapterView.OnItemClickListener() 
	        {
	        	@Override
				public void onItemClick(AdapterView<?> adapterView,	View view, int i, long l) 
	        	{   
	        		String usernamePicked = adapterView.getItemAtPosition(i).toString();
	        		
	        		Intent intent = new Intent(getBaseContext(), ProfileActivity.class);
	        		intent.putExtra("viewUserPicked", usernamePicked);
	        		startActivity(intent);
	        	}
			});
		}
	}
	
	private void fillCoachesList(String user)
	{
		ParseQuery<ParseObject> friendsQuery = ParseQuery.getQuery(UserLists.TABLE_NAME);
		friendsQuery.whereEqualTo(UserLists.USERNAME, user);
		ParseObject object = null;
		try {
			object = friendsQuery.getFirst();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<String> quals = object.getList(UserLists.MY_COACH_LIST);
		
		if(quals != null)
		{			
			ListAdapter techAdapt = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, quals);
	        ListView coachSelection = (ListView) findViewById(R.id.profile_coaches);
	        coachSelection.setAdapter(techAdapt);
	        
	        coachSelection.setOnItemClickListener(new AdapterView.OnItemClickListener() 
	        {
	        	@Override
				public void onItemClick(AdapterView<?> adapterView,	View view, int i, long l) 
	        	{   
	        		String usernamePicked = adapterView.getItemAtPosition(i).toString();
	        		
	        		Intent intent = new Intent(getBaseContext(), ProfileActivity.class);
	        		intent.putExtra("viewUserPicked", usernamePicked);
	        		startActivity(intent);
	        	}
			});
		}
	}
	
	private class SetProfileImage extends AsyncTask<String, Integer, Bitmap>
	{
		@Override
		protected Bitmap doInBackground(String... params) {
			return setProfileImage(params[0]);
		}
		
		@Override
		protected void onPostExecute(Bitmap result) {
			view.setImageBitmap(result);
		}
	}
	private Bitmap setProfileImage(String url) {
		Bitmap bm = null;
        try {
            URL aURL = new URL(url);
            URLConnection conn = aURL.openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            bm = BitmapFactory.decodeStream(bis);
            
            bis.close();
            is.close();
       } catch (IOException e) {
           Log.e(ProfileActivity.class.toString(), "Error getting bitmap", e);
       }
        return bm;
    }
	
	public void addCoachButtonClick(View v)
	{
		Button b = (Button) v;
		
		ParseUser currentUser = ParseUser.getCurrentUser();
		ParseQuery<ParseObject> coachesQuery = ParseQuery.getQuery(UserLists.TABLE_NAME);
		coachesQuery.whereEqualTo(UserLists.USERNAME, currentUser.getUsername());
		ParseObject object = null;
		try {
			object = coachesQuery.getFirst();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<String> coaches = object.getList(UserLists.MY_COACH_LIST);
		
		if(coaches == null)
		{
			List<String> newCoaches = new ArrayList<String>();
			newCoaches.add(usernamePicked);
			
			ParseQuery<ParseUser> query = ParseUser.getQuery();
			query.whereEqualTo(User.USERNAME, currentUser.getUsername());
			object.put(UserLists.MY_COACH_LIST, newCoaches);
			object.saveInBackground();
			
			coachAddBack(usernamePicked, currentUser.getUsername());
			
			b.setText("Added Coach");
			
			// Easter egg
			b.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					clickCount--;
					if(clickCount <= 5 && clickCount >= 0)
					{
						if(clickCount == 0)
						{
							Toast.makeText(getApplicationContext(), "BOOM!!\n(>'_')>>D )x_x)", Toast.LENGTH_LONG).show();
						}
						else
						{
							Toast.makeText(getApplicationContext(), "COUNTDOWN: " + clickCount, Toast.LENGTH_SHORT).show();
						}
					}
				}
			});
		}
		else
		{
			coaches.add(usernamePicked);
			object.put(UserLists.MY_COACH_LIST, coaches);
			object.saveInBackground();
			coachAddBack(usernamePicked, currentUser.getUsername());
			b.setText("Added Coach");
			
			// Easter egg
			b.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					clickCount--;
					if(clickCount <= 5 && clickCount >= 0)
					{
						if(clickCount == 0)
						{
							Toast.makeText(getApplicationContext(), "BOOM!!\n(>'_')>>D )x_x)", Toast.LENGTH_LONG).show();
						}
						else
						{
							Toast.makeText(getApplicationContext(), "COUNTDOWN: " + clickCount, Toast.LENGTH_SHORT).show();
						}
					}
				}
			});
		}
	}
	private void coachAddBack(String coachUsername, String currentUsername)
	{
		ParseQuery<ParseObject> coachesQuery = ParseQuery.getQuery(UserLists.TABLE_NAME);
		coachesQuery.whereEqualTo(UserLists.USERNAME, coachUsername);
		ParseObject object = null;
		try {
			object = coachesQuery.getFirst();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<String> coachsSwimmers = object.getList(UserLists.MY_SWIMMERS_LIST);
		
		if(coachsSwimmers != null)
		{
			coachsSwimmers.add(currentUsername);
			object.put(UserLists.MY_SWIMMERS_LIST, coachsSwimmers);
			object.saveInBackground();
		}
		else
		{
			List<String> newSwimmers = new ArrayList<String>();
			newSwimmers.add(currentUsername);
			object.put(UserLists.MY_SWIMMERS_LIST, newSwimmers);
			object.saveInBackground();
		}
	}
	
	public void addFriendButtonClick(View v)
	{
		Button b = (Button) v;
		
		ParseUser currentUser = ParseUser.getCurrentUser();
		ParseQuery<ParseObject> coachesQuery = ParseQuery.getQuery(UserLists.TABLE_NAME);
		coachesQuery.whereEqualTo(UserLists.USERNAME, currentUser.getUsername());
		ParseObject object = null;
		try {
			object = coachesQuery.getFirst();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<String> friends = object.getList(UserLists.MY_FRIENDS_LIST);
		
		if(friends == null)
		{
			List<String> newCoaches = new ArrayList<String>();
			newCoaches.add(usernamePicked);
			
			object.put(UserLists.MY_FRIENDS_LIST, newCoaches);
			object.saveInBackground();
			friendAddBack(usernamePicked, currentUser.getUsername());
			b.setText("Added Friend");
			
			b.setOnClickListener(null);
		}
		else
		{
			friends.add(usernamePicked);
			object.put(UserLists.MY_FRIENDS_LIST, friends);
			object.saveInBackground();
			friendAddBack(usernamePicked, currentUser.getUsername());
			b.setText("Added Friend");
			
			b.setOnClickListener(null);
		}
	}
	public void friendAddBack(String friendUsername, String currentUsername)
	{
		ParseQuery<ParseObject> coachesQuery = ParseQuery.getQuery(UserLists.TABLE_NAME);
		coachesQuery.whereEqualTo(UserLists.USERNAME, friendUsername);
		ParseObject object = null;
		try {
			object = coachesQuery.getFirst();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<String> friendsFriends = object.getList(UserLists.MY_FRIENDS_LIST);
		
		if(friendsFriends != null)
		{
			friendsFriends.add(currentUsername);
			object.put(UserLists.MY_FRIENDS_LIST, friendsFriends);
			object.saveInBackground();
		}
		else
		{
			List<String> newFriends = new ArrayList<String>();
			newFriends.add(currentUsername);
			object.put(UserLists.MY_FRIENDS_LIST, newFriends);
			object.saveInBackground();
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.profile, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		else if(id == R.id.action_logout)
		{
			ParseUser.logOut();
			ParseUser.getCurrentUser();
			Intent intent = new Intent(this, LogInActivity.class);
			startActivity(intent);
			finish();
		}
		else if(id == R.id.profile_edit)
		{			
			count++;
			
			int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getResources().getDisplayMetrics());
			int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, getResources().getDisplayMetrics());
			
			Drawable d = getResources().getDrawable(R.drawable.edit_pencil);
			
			ImageButton picEdit = (ImageButton) findViewById(R.id.profile_edit_picture);
			ImageView profilePic = (ImageView) findViewById(R.id.profile_picture);
			RelativeLayout.LayoutParams rllp = new RelativeLayout.LayoutParams(width, height);			
			rllp.leftMargin = profilePic.getRight() - d.getIntrinsicWidth() - (d.getIntrinsicWidth()/2);
			rllp.topMargin = profilePic.getBottom() - d.getIntrinsicHeight() - (d.getIntrinsicHeight()/2);
			
			ImageButton desEdit = (ImageButton) findViewById(R.id.profile_edit_picture);
//			ImageView profilePic = (ImageView) findViewById(R.id.profile_picture);
			
			ImageButton achEdit = (ImageButton) findViewById(R.id.profile_edit_picture);
//			ImageView profilePic = (ImageView) findViewById(R.id.profile_picture);
			
			ImageButton qualEdit = (ImageButton) findViewById(R.id.profile_edit_picture);
//			ImageView profilePic = (ImageView) findViewById(R.id.profile_picture);
			
			picEdit.setLayoutParams(rllp);
			picEdit.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					Toast.makeText(getBaseContext(), "something", Toast.LENGTH_LONG).show();
				}
			});
			
			if(count % 2 == 1)
			{
				picEdit.setVisibility(View.VISIBLE);
			}
			else
			{
				picEdit.setVisibility(View.GONE);
			}
		}
		return super.onOptionsItemSelected(item);
	}
}