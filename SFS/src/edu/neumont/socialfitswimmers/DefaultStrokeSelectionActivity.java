package edu.neumont.socialfitswimmers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.parse.Parse;
import com.parse.ParseUser;

public class DefaultStrokeSelectionActivity extends ActionBarActivity {
	
	public final static String EXTRA_TECHNIQUE = "edu.neumont.socialfitswimmers.TECHNIQUE";
	public final static String[] techniques = {"Butterfly", "Backstroke", "Breaststroke", "Freestyle"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stroke_selection);
        
        Parse.initialize(this, "KUOeDfzwtxhQ2ux0zGVgLnxO1XGtpNCS2YFIo065", "sCD4VCKI2J6BpyElGNGH89agLXziONgVh4odYqFU");
        
        ListAdapter techAdapt = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, techniques);
        ListView techniqueSelection = (ListView) findViewById(R.id.technique_selection);    
        techniqueSelection.setAdapter(techAdapt);
        
        final Context con = this;
        
        techniqueSelection.setOnItemClickListener(new AdapterView.OnItemClickListener() 
        {
        	@Override
			public void onItemClick(AdapterView<?> adapterView,	View view, int i, long l) 
        	{   
        		String techniquePicked = adapterView.getItemAtPosition(i).toString();
        		
        		Intent intent = new Intent(con, YoutubeActivity.class);
        		intent.putExtra(EXTRA_TECHNIQUE, techniquePicked);
        		startActivity(intent);
        	} 
		}); 
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id == R.id.action_logout)
		{
			ParseUser.logOut();
			ParseUser.getCurrentUser();
			Intent intent = new Intent(this, LogInActivity.class);
			startActivity(intent);
			finish();
		}
        return super.onOptionsItemSelected(item);
    }
}