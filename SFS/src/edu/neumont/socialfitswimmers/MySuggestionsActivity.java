package edu.neumont.socialfitswimmers;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import edu.neumont.Models.Suggestions;
import edu.neumont.swimmer_account.SuggestionsActivity;

public class MySuggestionsActivity extends ActionBarActivity {
	
	public ParseUser currentUser;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_my_suggestions);
		
		Parse.initialize(this, "KUOeDfzwtxhQ2ux0zGVgLnxO1XGtpNCS2YFIo065", "sCD4VCKI2J6BpyElGNGH89agLXziONgVh4odYqFU");
		currentUser = ParseUser.getCurrentUser();
	}
	
	public void butterflySuggestionSubmit(View v)
	{
		EditText edit = (EditText) findViewById(R.id.suggestions_butterfly_input);
		ParseQuery<ParseObject> query = ParseQuery.getQuery(Suggestions.TABLE_NAME);
		query.whereEqualTo(Suggestions.USERNAME, currentUser.getUsername());
		
		ParseObject object = null; 
		try {
			object = query.getFirst();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<String> suggestions = object.getList(Suggestions.BUTTERFLY_SUGGESTIONS);
		if(suggestions != null)
		{
			suggestions.add(edit.getText().toString());
			object.put(Suggestions.BUTTERFLY_SUGGESTIONS, suggestions);
			object.saveInBackground();
		}
		else
		{
			List<String> sugs = new ArrayList<String>();
			sugs.add(edit.getText().toString());
			object.put(Suggestions.BUTTERFLY_SUGGESTIONS, sugs);
			object.saveInBackground();
		}
		edit.setText("");
		Toast.makeText(this, "Successfully added your Butterfly suggestion!", Toast.LENGTH_SHORT).show();
	}
	
	public void backstrokeSuggestionSubmit(View v)
	{
		EditText edit = (EditText) findViewById(R.id.suggestions_backstroke_input);
		ParseQuery<ParseObject> query = ParseQuery.getQuery(Suggestions.TABLE_NAME);
		query.whereEqualTo(Suggestions.USERNAME, currentUser.getUsername());
		
		ParseObject object = null; 
		try {
			object = query.getFirst();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<String> suggestions = object.getList(Suggestions.BACKSTROKE_SUGGESTIONS);
		if(suggestions != null)
		{
			suggestions.add(edit.getText().toString());		
			object.put(Suggestions.BACKSTROKE_SUGGESTIONS, suggestions);
			object.saveInBackground();
		}
		else
		{
			List<String> sugs = new ArrayList<String>();
			sugs.add(edit.getText().toString());
			object.put(Suggestions.BACKSTROKE_SUGGESTIONS, sugs);
			object.saveInBackground();
		}
		edit.setText("");
		Toast.makeText(this, "Successfully added your Backstroke suggestion!", Toast.LENGTH_SHORT).show();
	}
	
	public void breaststrokeSuggestionSubmit(View v)
	{
		EditText edit = (EditText) findViewById(R.id.suggestions_breaststroke_input);
		ParseQuery<ParseObject> query = ParseQuery.getQuery(Suggestions.TABLE_NAME);
		query.whereEqualTo(Suggestions.USERNAME, currentUser.getUsername());
		
		ParseObject object = null; 
		try {
			object = query.getFirst();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<String> suggestions = object.getList(Suggestions.BREASTSTROKE_SUGGESTIONS);
		if(suggestions != null)
		{
			suggestions.add(edit.getText().toString());		
			object.put(Suggestions.BREASTSTROKE_SUGGESTIONS, suggestions);
			object.saveInBackground();
		}
		else
		{
			List<String> sugs = new ArrayList<String>();
			sugs.add(edit.getText().toString());
			object.put(Suggestions.BREASTSTROKE_SUGGESTIONS, sugs);
			object.saveInBackground();
		}
		edit.setText("");
		Toast.makeText(this, "Successfully added your Breaststroke suggestion!", Toast.LENGTH_SHORT).show();
	}
	
	public void freestyleSuggestionSubmit(View v)
	{
		EditText edit = (EditText) findViewById(R.id.suggestions_freestyle_input);
		ParseQuery<ParseObject> query = ParseQuery.getQuery(Suggestions.TABLE_NAME);
		query.whereEqualTo(Suggestions.USERNAME, currentUser.getUsername());
		
		ParseObject object = null; 
		try {
			object = query.getFirst();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		List<String> suggestions = object.getList(Suggestions.FREESTYLE_SUGGESTIONS);
		if(suggestions != null)
		{
			suggestions.add(edit.getText().toString());		
			object.put(Suggestions.FREESTYLE_SUGGESTIONS, suggestions);
			object.saveInBackground();
		}
		else
		{
			List<String> sugs = new ArrayList<String>();
			sugs.add(edit.getText().toString());
			object.put(Suggestions.FREESTYLE_SUGGESTIONS, sugs);
			object.saveInBackground();
		}
		edit.setText("");
		Toast.makeText(this, "Successfully added your Freestyle suggestion!", Toast.LENGTH_SHORT).show();
	}
	
	public void viewMyButterflySuggestions(View v)
	{
		Intent intent = new Intent(this, SuggestionsActivity.class);
		intent.putExtra("techniquePicked", "Butterfly");
		intent.putExtra("userPicked", currentUser.getUsername());
		startActivity(intent);
	}
	
	public void viewMyBackstrokeSuggestions(View v)
	{
		Intent intent = new Intent(this, SuggestionsActivity.class);
		intent.putExtra("techniquePicked", "Backstroke");
		intent.putExtra("userPicked", currentUser.getUsername());
		startActivity(intent);
	}
	
	public void viewMyBreaststrokeSuggestions(View v)
	{
		Intent intent = new Intent(this, SuggestionsActivity.class);
		intent.putExtra("techniquePicked", "Breaststroke");
		intent.putExtra("userPicked", currentUser.getUsername());
		startActivity(intent);
	}
	
	public void viewMyFreestyleSuggestions(View v)
	{
		Intent intent = new Intent(this, SuggestionsActivity.class);
		intent.putExtra("techniquePicked", "Freestyle");
		intent.putExtra("userPicked", currentUser.getUsername());
		startActivity(intent);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.my_suggestions, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}