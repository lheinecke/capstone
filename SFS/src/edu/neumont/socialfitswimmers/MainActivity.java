package edu.neumont.socialfitswimmers;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.parse.Parse;
import com.parse.ParseUser;
import edu.neumont.Models.User;
import edu.neumont.swimmer_account.SwimmerStrokeSelectionActivity;

public class MainActivity extends ActionBarActivity {
	
	private ParseUser currentUser;
	private String currentUserAccountType;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Parse.initialize(this, "KUOeDfzwtxhQ2ux0zGVgLnxO1XGtpNCS2YFIo065", "sCD4VCKI2J6BpyElGNGH89agLXziONgVh4odYqFU");
        
        TextView strokeSelect = (TextView) findViewById(R.id.stroke_select);
        strokeSelect.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        
        currentUser = ParseUser.getCurrentUser();
        currentUserAccountType = currentUser.getString(User.ACCOUNT_TYPE);
        
        strokeSelect.setText("Welcome " + currentUser.getUsername() + "!");
        
		if(currentUserAccountType.equals(User.ACCOUNT_TYPE_COACH))
		{
			Button b = (Button) findViewById(R.id.main_coach_suggestions);
			b.setVisibility(Button.GONE);
		}
    }
    
    // My Suggestions Button
    public void suggestionsButton(View v)
    {
    	Intent intent = new Intent(this, MySuggestionsActivity.class);
    	startActivity(intent);
    }
    
    // Coach Suggestions Button
    public void swimmerSuggestionsButton(View v)
    {
    	Intent intent = new Intent(this, SwimmerStrokeSelectionActivity.class);
    	intent.putExtra("listChoice", "coach");
    	startActivity(intent);
    }
    
    // Friend Suggestions Button
    public void friendSuggestionsButton(View v)
    {		
    	Intent intent = new Intent(this, SwimmerStrokeSelectionActivity.class);
    	intent.putExtra("listChoice", "friend");
    	startActivity(intent);
    }
    
    public void messagingButton(View v)
    {
    	Intent intent = new Intent(this, MessageUserChoiceActivity.class);
    	startActivity(intent);
    }
    
    public void myProfile(View v)
    {
    	Intent intent = new Intent(this, ProfileActivity.class);
    	intent.putExtra("viewUserPicked", currentUser.getUsername());
    	startActivity(intent);
    }
    
    public void trainingButton(View v)
    {
    	Intent intent = new Intent(this, DefaultStrokeSelectionActivity.class);
    	startActivity(intent);
    }
    
    public void findCoaches(View v)
    {				
		Intent intent = new Intent(this, FindCoachActivity.class);
		startActivity(intent);
    }
    
    public void viewCoaches(View v)
    {
    	Intent intent = new Intent(this, ViewCoachesActivity.class);
    	startActivity(intent);
    }
    
    public void findFriends(View v)
    {
    	Intent intent = new Intent(this, FindFriendActivity.class);
		startActivity(intent);
    }
    
    public void viewFriends(View v)
    {
    	Intent intent = new Intent(this, ViewFriendsActivity.class);
    	startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        else if(id == R.id.action_logout)
		{
			ParseUser.logOut();
			ParseUser.getCurrentUser();
			Intent intent = new Intent(this, LogInActivity.class);
			startActivity(intent);
			finish();
		}
        return super.onOptionsItemSelected(item);
    }
}