package edu.neumont.socialfitswimmers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseUser;

public class LogInActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_log_in);
		
		Parse.initialize(this, "KUOeDfzwtxhQ2ux0zGVgLnxO1XGtpNCS2YFIo065", "sCD4VCKI2J6BpyElGNGH89agLXziONgVh4odYqFU");
		
		ParseUser currentUser = null;
		currentUser = ParseUser.getCurrentUser();
		if(currentUser != null && currentUser.getUsername() != null)
		{
			Intent intent = new Intent(getBaseContext(), MainActivity.class);
			startActivity(intent);
			
			currentUser.fetchInBackground(new GetCallback<ParseUser>() {
				@Override
				public void done(ParseUser user, ParseException e) {
					if(e == null) {
						// Success!
					} else {
						// Failure!
					}
				}
			});
			finish();
		}
	}
	
	public void loginButtonClick(View v)
	{
		EditText user = (EditText) findViewById(R.id.username_login_input);
		EditText pass = (EditText) findViewById(R.id.password_login_input);
		
		String username = user.getText().toString();
		String password = pass.getText().toString();
		
		ParseUser.logInInBackground(username, password, new LogInCallback() 
		{	
			@Override
			public void done(ParseUser user, ParseException e) 
			{
				if(user != null)
				{
					Intent intent = new Intent(getBaseContext(), MainActivity.class);
					startActivity(intent);
					finish();
				}
				else
				{
					Toast.makeText(getBaseContext(), "Failed to log in! Please try again!", Toast.LENGTH_LONG).show();
				}
			}
		});
	}
	
	public void signUpButtonClick(View v)
	{
		Intent intent = new Intent(this, SignUpActivity.class);
		startActivity(intent);
		finish();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.log_in, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}