package edu.neumont.socialfitswimmers;

import java.util.Timer;
import java.util.TimerTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;

public class YoutubeActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener
{	
	private final String DEVELOPER_KEY = "AIzaSyBRCUOooHFUMMpBBkYhoZvkwFljj51IRZk";
	private String VIDEO = "";
	private String option;
	private Timer timer;
	int count = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		option = getIntent().getStringExtra(DefaultStrokeSelectionActivity.EXTRA_TECHNIQUE);
		
		if(option.equals(DefaultStrokeSelectionActivity.techniques[0])) // Butterfly
		{
			VIDEO = "w6n_SrjLypA";
		}
		else if(option.equals(DefaultStrokeSelectionActivity.techniques[1])) // Backstroke
		{
			VIDEO = "McC3crRWpGc";
		}
		else if(option.equals(DefaultStrokeSelectionActivity.techniques[2])) // Breaststroke
		{
			VIDEO = "y11Hb5m-nU4";
		}
		else if(option.equals(DefaultStrokeSelectionActivity.techniques[3])) // Freestyle
		{
			VIDEO = "-Wei4DGNYJM";
		}
		
		setContentView(R.layout.youtube_tutorial_video);
		
		YouTubePlayerView youtubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
		
		youtubeView.initialize(DEVELOPER_KEY, this);
	}

	@Override
	public void onInitializationFailure(Provider provider, YouTubeInitializationResult result) 
	{
		Toast.makeText(this, "Error: Please download/update the latest version of the Youtube application on your phone!", Toast.LENGTH_LONG).show();
	}

	@Override
	public void onInitializationSuccess(Provider provider, YouTubePlayer player, boolean wasRestored)
	{
		final YouTubePlayer myPlayer = player;
		
		if (!wasRestored)
		{
			player.cueVideo(VIDEO);
		}
		else
		{
			player.loadVideo(VIDEO);
		}
		
		final Handler updateTextView = new Handler(new Callback()
		{
			@Override
			public boolean handleMessage(Message msg) 
			{
				TextView view = (TextView) findViewById(R.id.video_description);
				view.setText(msg.obj.toString());
				return false;
			}
		});
		
		TimerTask task = new TimerTask()
		{
			@Override
			public void run()
			{
				if(myPlayer != null && myPlayer.isPlaying())
				{
					int videoTime = myPlayer.getCurrentTimeMillis();
					
					if(option.equals(DefaultStrokeSelectionActivity.techniques[0])) // Butterfly
					{
						if((videoTime >= 3000 && videoTime <= 45500))
						{
							Message msg = updateTextView.obtainMessage();
							msg.obj = "Notice how he is using both his arms to move forward,"
									+ " when his arms are in-front of him he does a small kick. "
									+ " And when his arms are below him he does a powerful kick in "
									+ "order for him to come up and catch his breath!";
							updateTextView.sendMessage(msg);
						}
						else if(videoTime >= 46000 && videoTime <= 72500)
						{
							Message msg = updateTextView.obtainMessage();
							msg.obj = "Also look at how he has his arms are pointed in front of him,"
									+ " hand over hand.  This is called Streamline and it should be"
									+ " done at any time you push/kick off of a wall or dive off of "
									+ "your start.";
							updateTextView.sendMessage(msg);
						}
						else if(videoTime >= 73000 && videoTime <= 110000)
						{
							Message msg = updateTextView.obtainMessage();
							msg.obj = "Notice when he pushes off of the wall, he uses both his legs "
									+ "to kick in a dolphin motion, hence the name dolphin kick! "
									+ "And notice how he is using his abdominals to power his "
									+ "Dolphin Kick.  You want a strong kick so that you can go "
									+ "underwater farther and not have to swim for so long";
							updateTextView.sendMessage(msg);
						}
						else if(videoTime >= 115000)
						{
							timer.cancel();
						}
					}
					else if(option.equals(DefaultStrokeSelectionActivity.techniques[1])) // Backstroke
					{
						if((videoTime >= 3000 && videoTime <= 18500))
						{
							Message msg = updateTextView.obtainMessage();
							msg.obj = "Notice how he is using his Streamline to push off of the wall";
							updateTextView.sendMessage(msg);
						}
						else if(videoTime >= 19000 && videoTime <= 39000)
						{
							Message msg = updateTextView.obtainMessage();
							msg.obj = "Notice how he is using his dolphin kick to push himself "
									+ "underwater.  Once he reached the "
									+ "surface he started doing his Flutter kick, kicking both "
									+ "legs, and moving his arms close to his ear and rotating "
									+ "his upperbody according to which arm is coming out of "
									+ "the water";
							updateTextView.sendMessage(msg);
						}
						else if(videoTime >= 39500)
						{
							timer.cancel();
						}
					}
					else if(option.equals(DefaultStrokeSelectionActivity.techniques[2])) // Breaststroke
					{
						if((videoTime >= 10000 && videoTime <= 16500))
						{
							Message msg = updateTextView.obtainMessage();
							msg.obj = "Notice when he is bringing his arms up to his body, he"
									+ " brings them around and up to his chest!";
							updateTextView.sendMessage(msg);
						}
						else if(videoTime >= 17000 && videoTime <= 52500)
						{
							Message msg = updateTextView.obtainMessage();
							msg.obj = "Notice when touched the wall he touched with two hands "
									+ "and then brought one hand over his head and the other "
									+ "tucked in under him and then pushed off the wall";
							updateTextView.sendMessage(msg);
						}
						else if(videoTime >= 53000 && videoTime <= 62000)
						{
							Message msg = updateTextView.obtainMessage();
							msg.obj = "Notice how he moves his legs!  The reason he moves his "
									+ "legs that way is because it gives power to his stroke!";
							updateTextView.sendMessage(msg);
						}
						else if(videoTime >= 62500)
						{
							timer.cancel();
						}
					}
					else if(option.equals(DefaultStrokeSelectionActivity.techniques[3])) // Freestyle
					{
						if((videoTime >= 6000 && videoTime <= 24500))
						{
							Message msg = updateTextView.obtainMessage();
							msg.obj = "Notice how he is kicking his legs, this is called a flutter kick";
							updateTextView.sendMessage(msg);
						}
						else if(videoTime >= 25000 && videoTime <= 53500)
						{
							Message msg = updateTextView.obtainMessage();
							msg.obj = "Notice how he is moving his arms, his arms just come in a "
									+ "rotational motion and he brings his arms straight down "
									+ "beside him";
							updateTextView.sendMessage(msg);
						}
						else if(videoTime >= 54000 && videoTime <= 72000)
						{
							Message msg = updateTextView.obtainMessage();
							msg.obj = "Notice how he is kicking and moving his arms really fast! "
									+ " You want speed in your motions when swimming freestyle "
									+ "to better your time!";
							updateTextView.sendMessage(msg);
						}
						else if(videoTime >= 72500)
						{
							timer.cancel();
						}
					}
				}
			}
		};	
		timer = new Timer("VideoTimer");
		timer.scheduleAtFixedRate(task, 0, 100);
	}
	
	@Override
	protected void onPause()
	{
		timer.cancel();
		super.onPause();
	}
	
	@Override
	protected void onDestroy()
	{
		timer.cancel();
		super.onDestroy();
	}
}