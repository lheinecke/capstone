//package edu.neumont.backgroundoperations;
//
//import java.util.ArrayList;
//import java.util.List;
//
//import com.parse.ParseException;
//import com.parse.ParseObject;
//import com.parse.ParseQuery;
//import com.parse.ParseUser;
//
//import edu.neumont.Models.Chat;
//
//public class NewMessageChecker implements Runnable 
//{	
//	private ParseUser otherUser;
//	private ParseObject threadChat;
//	private ParseObject mostRecentChat;
//	
//	public NewMessageChecker(ParseUser otherUser, ParseObject mostRecentChat)
//	{
//		this.otherUser = otherUser;
//		this.mostRecentChat = mostRecentChat;
//	}
//	
//	@Override
//	public void run() 
//	{
//		android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
//		
//		String userPicked = otherUser.getUsername();
//		
//		ParseQuery<ParseObject> chatQuery = ParseQuery.getQuery(Chat.TABLE_NAME);
//		chatQuery.whereEqualTo(Chat.USER1, userPicked);
//		chatQuery.whereEqualTo(Chat.USER2, ParseUser.getCurrentUser().getUsername());
//		
//		ParseQuery<ParseObject> chatQuery2 = ParseQuery.getQuery(Chat.TABLE_NAME);
//		chatQuery2.whereEqualTo(Chat.USER2, userPicked);
//		chatQuery2.whereEqualTo(Chat.USER1, ParseUser.getCurrentUser().getUsername());
//		
//		List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();
//		queries.add(chatQuery);
//		queries.add(chatQuery2);
//		
//		ParseQuery<ParseObject> result = ParseQuery.or(queries);
//		try {
//			threadChat = result.getFirst();
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		
//		System.out.println("Thread chat size: " + threadChat.getList(Chat.MESSAGES).size());
//		System.out.println("Most recent chat size: " + mostRecentChat.getList(Chat.MESSAGES).size());
//		if(threadChat.getList(Chat.MESSAGES).size() > mostRecentChat.getList(Chat.MESSAGES).size())
//		{
//			System.out.println("HELLO!!");
//			mostRecentChat = threadChat;
//		}
//		System.out.println("returned");
//	}
//}