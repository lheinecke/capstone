//package edu.neumont.backgroundoperations;
//
//import java.util.ArrayList;
//import java.util.List;
//import com.parse.ParseException;
//import com.parse.ParseObject;
//import com.parse.ParseQuery;
//import com.parse.ParseUser;
//import com.parse.SaveCallback;
//import edu.neumont.Models.Chat;
//import edu.neumont.Models.MyMessage;
//
//public class SendMessageRunnable implements Runnable
//{
//	private Runnable messageRunnable;
//	private ParseUser currentUser;
//	private ParseUser otherUser;
//	private String message;
//	
//	public SendMessageRunnable(ParseUser otherUser, String message)
//	{
//		messageRunnable = Thread.currentThread();
//		currentUser = ParseUser.getCurrentUser();
//		this.otherUser = otherUser;
//		this.message = message;
//	}
//			
//	@Override
//	public void run()
//	{
//		android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
//		
//		final ParseObject message = new ParseObject(MyMessage.TABLE_NAME);
//		message.put(MyMessage.CREATOR_USERNAME, currentUser.getUsername());
//		message.put(MyMessage.RECEIVER_USERNAME, otherUser.getUsername());
//		message.put(MyMessage.MESSAGE, message);
//		message.saveInBackground(new SaveCallback() {
//			@Override
//			public void done(ParseException e) {
//				System.out.println("MESSAGE SAVED");
//				final ParseObject chat = updateLatestChat();
//				List<String> messages = chat.getList(Chat.MESSAGES);
//				
//				messages.add(message.getObjectId());
//				chat.put(Chat.MESSAGES, messages);
//				System.out.println("CHAT SAVING");
//				chat.saveInBackground(new SaveCallback() {					
//					@Override
//					public void done(ParseException e) {
//						System.out.println("CHAT SAVED");
//						new updateUITask().execute(updateLatestChat());
//						input.setText("");
//					}
//				});
//			}
//		});
//	}
//	
//	private ParseObject updateLatestChat()
//	{	
//		ParseObject mostRecentChat = null;
//		String userPicked = otherUser.getUsername();
//		
//		ParseQuery<ParseObject> chatQuery = ParseQuery.getQuery(Chat.TABLE_NAME);
//		chatQuery.whereEqualTo(Chat.USER1, userPicked);
//		chatQuery.whereEqualTo(Chat.USER2, ParseUser.getCurrentUser().getUsername());
//		
//		ParseQuery<ParseObject> chatQuery2 = ParseQuery.getQuery(Chat.TABLE_NAME);
//		chatQuery2.whereEqualTo(Chat.USER2, userPicked);
//		chatQuery2.whereEqualTo(Chat.USER1, ParseUser.getCurrentUser().getUsername());
//		
//		List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();
//		queries.add(chatQuery);
//		queries.add(chatQuery2);
//		
//		ParseQuery<ParseObject> result = ParseQuery.or(queries);
//		try {
//			mostRecentChat = result.getFirst();
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		return mostRecentChat;
//	}
//}