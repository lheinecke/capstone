//package edu.neumont.backgroundoperations;
//
//import java.util.concurrent.BlockingQueue;
//import java.util.concurrent.LinkedBlockingQueue;
//import java.util.concurrent.ThreadPoolExecutor;
//import java.util.concurrent.TimeUnit;
//import android.os.Handler;
//import android.os.Looper;
//import android.os.Message;
//import com.parse.ParseUser;
//
//public class ChatManager
//{
//	private BlockingQueue<Runnable> sendMessageQueue;
//	private ThreadPoolExecutor sendMessageThreadPool;
//	private static ChatManager cInstance = null;
//	private Handler mHandler;
//
//    // Sets the amount of time an idle thread will wait for a task before terminating
//    private static final int KEEP_ALIVE_TIME = 1;
//
//    // Sets the Time Unit to seconds    
//    private static final TimeUnit KEEP_ALIVE_TIME_UNIT;
//    
//    // Sets the initial threadpool size to 8    
//    private static final int CORE_POOL_SIZE = 8;
//    
//    // Sets the maximum threadpool size to 8
//    private static final int MAXIMUM_POOL_SIZE = 8;
//
//    /**
//     * NOTE: This is the number of total available cores. On current versions of
//     * Android, with devices that use plug-and-play cores, this will return less
//     * than the total number of cores. The total number of cores is not
//     * available in current Android implementations.
//     */
//    private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
//	
//	static{
//		KEEP_ALIVE_TIME_UNIT = TimeUnit.SECONDS;
//		cInstance = new ChatManager();
//	}
//	
//	private ChatManager()
//	{
//		sendMessageQueue = new LinkedBlockingQueue<Runnable>();
//		sendMessageThreadPool = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, 
//			KEEP_ALIVE_TIME, KEEP_ALIVE_TIME_UNIT, sendMessageQueue);
//		mHandler = new Handler(Looper.getMainLooper())
//		{
//			@Override
//			public void handleMessage(Message msg)
//			{
//				
//				super.handleMessage(msg);
//			}
//		};
//	}
//	
//	public void sendMessage(ParseUser otherUser, String msg)
//	{
//		cInstance.sendMessageThreadPool.execute(new SendMessageRunnable(otherUser, msg));
//	}
//}