//package edu.neumont.backgroundoperations;
//
//import java.util.ArrayList;
//import java.util.List;
//import com.parse.ParseException;
//import com.parse.ParseObject;
//import com.parse.ParseQuery;
//import com.parse.ParseUser;
//import edu.neumont.Models.Chat;
//import edu.neumont.messaging.MessageChatActivity.ResponseReceiver;
//import android.app.IntentService;
//import android.content.Intent;
//
//public class MyChatIntentService extends IntentService
//{
//	public static final String PARAM_IN_CHAT = "ichat";
//	public static final String PARAM_OUT_CHAT = "ochat";
//	public static final String PARAM_IN_USER_PICKED = "iuserpicked";
//	private String userPicked;
//	
//	public MyChatIntentService()
//	{
//		super("MyChatIntentService");
//	}
//
//	@Override
//	protected void onHandleIntent(Intent intent)
//	{
//		userPicked = intent.getStringExtra(PARAM_IN_USER_PICKED);
//		int receivingChatSize = intent.getIntExtra(PARAM_IN_CHAT, 0);
//		ParseObject mostRecentChat = getLatestChat();
//		int mostRecentChatSize = mostRecentChat.getList(Chat.MESSAGES).size();
//		
//		if(receivingChatSize < mostRecentChatSize)
//		{
//			Intent broadcastIntent = new Intent();
//			broadcastIntent.setAction(ResponseReceiver.ACTION_RESPONSE);
//			broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
//			broadcastIntent.putExtra(PARAM_OUT_CHAT, "updateChat");
//			sendBroadcast(broadcastIntent);
//		}
//		else
//		{
//			Intent broadcastIntent = new Intent();
//			broadcastIntent.setAction(ResponseReceiver.ACTION_RESPONSE);
//			broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
//			broadcastIntent.putExtra(PARAM_OUT_CHAT, "noUpdateChat");
//			sendBroadcast(broadcastIntent);
//		}
//	}
//	
//	private ParseObject getLatestChat()
//	{
//		ParseObject mostRecentChat = null;
//		
//		ParseQuery<ParseObject> chatQuery = ParseQuery.getQuery(Chat.TABLE_NAME);
//		chatQuery.whereEqualTo(Chat.USER1, userPicked);
//		chatQuery.whereEqualTo(Chat.USER2, ParseUser.getCurrentUser().getUsername());
//		
//		ParseQuery<ParseObject> chatQuery2 = ParseQuery.getQuery(Chat.TABLE_NAME);
//		chatQuery2.whereEqualTo(Chat.USER2, userPicked);
//		chatQuery2.whereEqualTo(Chat.USER1, ParseUser.getCurrentUser().getUsername());
//		
//		List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();
//		queries.add(chatQuery);
//		queries.add(chatQuery2);
//		
//		ParseQuery<ParseObject> result = ParseQuery.or(queries);
//		try {
//			mostRecentChat = result.getFirst();
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
//		
//		return mostRecentChat;
//	}
//}