package edu.neumont.Models;

public class User 
{
	public static final String TABLE_NAME = "User";
	public static final String ID = "objectId";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String DESCRIPTION = "description";
	public static final String ACHIEVEMENTS = "achievements";
	public static final String COACH_QUALIFICATIONS = "coachQualifications";
	public static final String PROFILE_IMAGE_URL = "profileImageUrl";
	public static final String ACCOUNT_TYPE = "accountType";
	public static final String ACCOUNT_TYPE_SWIMMER = "Swimmer";
	public static final String ACCOUNT_TYPE_COACH = "Coach";
}