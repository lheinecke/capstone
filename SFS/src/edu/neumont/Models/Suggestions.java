package edu.neumont.Models;

public class Suggestions
{
	public static final String TABLE_NAME = "Suggestions";
	public static final String ID = "objectId";
	public static final String USERNAME = "username";
	public static final String BUTTERFLY_SUGGESTIONS = "myButterflySuggestions";
	public static final String BACKSTROKE_SUGGESTIONS = "myBackstrokeSuggestions";
	public static final String BREASTSTROKE_SUGGESTIONS = "myBreaststrokeSuggestions";
	public static final String FREESTYLE_SUGGESTIONS = "myFreestyleSuggestions";
}