package edu.neumont.Models;

public class MyMessage 
{
	public static final String TABLE_NAME = "Message";
	public static final String ID = "objectId";
	public static final String CREATOR_USERNAME = "creatorUsername";
	public static final String RECEIVER_USERNAME = "receiverUsername";
	public static final String MESSAGE = "message";
	public static final String IS_GROUP = "isGroup";
	public static final String ATTACHMENT = "attachment";
	public static final String CHAT_PARENT = "chatParent";
}