package edu.neumont.Models;

public class UserLists 
{
	public static final String TABLE_NAME = "UserLists";
	public static final String ID = "objectId";
	public static final String USERNAME = "username";
	public static final String MY_COACH_LIST = "myCoachList";
	public static final String MY_FRIENDS_LIST = "myFriendList";
	public static final String MY_SWIMMERS_LIST = "mySwimmersList";
}