package edu.neumont.Messaging;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;
import edu.neumont.Models.Chat;
import edu.neumont.Models.MyMessage;
import edu.neumont.Models.User;
import edu.neumont.socialfitswimmers.LogInActivity;
import edu.neumont.socialfitswimmers.R;

public class MessageChatActivity extends ActionBarActivity {
	
	private ParseUser currentUser;
	private ParseUser otherUser;
	private static ParseObject mostRecentChat;
	private final Handler listHandler = new Handler();
	private boolean loop = true;
	
	final Runnable updateRunnable = new Runnable()
	{
		public void run()
		{
			System.out.println("Running updateRunnable");
			
			ParseRelation<ParseObject> relation = mostRecentChat.getRelation(MyMessage.TABLE_NAME);
			List<ParseObject> messageObjects = null;
			try
			{
				messageObjects = relation.getQuery().find();
			}
			catch (ParseException e1)
			{
				e1.printStackTrace();
			}
			
			Collections.sort(messageObjects, new Comparator<ParseObject>()
			{
				@Override
				public int compare(ParseObject lhs, ParseObject rhs)
				{
					Date d1 = lhs.getCreatedAt();
					Date d2 = rhs.getCreatedAt();
					
					return d1.compareTo(d2);
				}
			});
			
			List<Map<String, String>> result = getLatestChatAdapterFromDatabase(messageObjects);
			updateListViewAdapter(result);
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_message_chat);
		
		Parse.initialize(this, "KUOeDfzwtxhQ2ux0zGVgLnxO1XGtpNCS2YFIo065", "sCD4VCKI2J6BpyElGNGH89agLXziONgVh4odYqFU");
		
		currentUser = ParseUser.getCurrentUser();
		
		final String userPicked = getIntent().getStringExtra("userPicked");
		TextView userPickedText = (TextView) findViewById(R.id.message_chat_user);
		userPickedText.setText(userPicked);
		
		ParseQuery<ParseUser> otherQuery = ParseUser.getQuery();
		otherQuery.whereEqualTo(User.USERNAME, userPicked);
		try {
			otherUser = otherQuery.getFirst();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println("Initializing mostRecentChat: " + mostRecentChat);
		updateLatestChat();
		
		listHandler.post(new Runnable()
		{
			@Override
			public void run()
			{
				System.out.println("Should be initialized: " + mostRecentChat);
				System.out.println("Running initial mostRecentChat thread in onCreate");
				if(mostRecentChat == null)
				{
					ParseObject newChat = new ParseObject(Chat.TABLE_NAME);
					newChat.put(Chat.USER1, currentUser.getUsername());
					newChat.put(Chat.USER2, userPicked);
					try
					{
						newChat.save();
					}
					catch (ParseException e)
					{
						e.printStackTrace();
					}
					mostRecentChat = newChat;
					System.out.println("mostRecent = newChat");
				}
				else
				{
					listHandler.post(updateRunnable);
				}
			}
		});
		
		messageCheckAndUpdate();
	}
	
	@Override
	protected void onPause()
	{
		loop = false;
		super.onPause();
	}
	
	@Override
	protected void onDestroy()
	{
		loop = false;
		super.onDestroy();
	}
	
	private void messageCheckAndUpdate()
	{
		listHandler.post(updateRunnable);
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				ParseObject threadChat = mostRecentChat;
				while(loop)
				{
					try
					{
						Thread.sleep(5000);
					}
					catch (InterruptedException e)
					{
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if(threadChat != null)
					{
						System.out.println("updateRunnable in front of queue");
						listHandler.postAtFrontOfQueue(updateRunnable);
						System.out.println("Just posted to handler");
					}
					else
					{
						threadChat = mostRecentChat;
					}
				}
			}
		}).start();
	}
	
	private void updateLatestChat()
	{
		listHandler.post(new Runnable(){
			@Override
			public void run()
			{
				System.out.println("updating latest chat");
				String userPicked = otherUser.getUsername();
				
				ParseQuery<ParseObject> chatQuery = ParseQuery.getQuery(Chat.TABLE_NAME);
				chatQuery.whereEqualTo(Chat.USER1, userPicked);
				chatQuery.whereEqualTo(Chat.USER2, ParseUser.getCurrentUser().getUsername());
				
				ParseQuery<ParseObject> chatQuery2 = ParseQuery.getQuery(Chat.TABLE_NAME);
				chatQuery2.whereEqualTo(Chat.USER2, userPicked);
				chatQuery2.whereEqualTo(Chat.USER1, ParseUser.getCurrentUser().getUsername());
				
				List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();
				queries.add(chatQuery);
				queries.add(chatQuery2);
				
				ParseQuery<ParseObject> result = ParseQuery.or(queries);
				try {
					mostRecentChat = result.getFirst();
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	private List<Map<String, String>> getLatestChatAdapterFromDatabase(List<ParseObject> messageObjects)
	{
		List<Map<String, String>> data = new ArrayList<Map<String, String>>();
		
		if((messageObjects != null) && (messageObjects.size() != 0))
		{			
			for (ParseObject o : messageObjects) 
			{
			    Map<String, String> datum = new HashMap<String, String>(2);
			    Date date = o.getCreatedAt();
			    Calendar cal = Calendar.getInstance();
			    cal.setTime(date);
			    int month = cal.get(Calendar.MONTH)+1;
			    int day = cal.get(Calendar.DAY_OF_MONTH);
			    int year = cal.get(Calendar.YEAR);
			    int hour = cal.get(Calendar.HOUR);
			    int minute = cal.get(Calendar.MINUTE);
			    String dateFormatted = "";
			    
			    if(minute < 10)
			    {
			    	dateFormatted = month+"/"+day+"/"+year+" at "+hour+":0"+minute;
			    }
			    else
			    {
			    	dateFormatted = month+"/"+day+"/"+year+" at "+hour+":"+minute;
			    }
			    
			    datum.put("message", o.getString(MyMessage.MESSAGE));
			    datum.put("sentBy", o.getString(MyMessage.CREATOR_USERNAME) + "\n" + dateFormatted);
			    data.add(datum);
			}
		}
		return data;
	}
	
	private void updateListViewAdapter(List<Map<String, String>> result)
	{
		final List<Map<String, String>> realResult = result;
		listHandler.post(new Runnable()
		{
			@Override
			public void run()
			{
				System.out.println("Updating adapter");
				ListView chatView = (ListView) findViewById(R.id.message_chat_messages_view);
				
				final SimpleAdapter adapter = new SimpleAdapter(getBaseContext(), realResult,
		                android.R.layout.simple_list_item_2,
		                new String[] {"message", "sentBy"},
		                new int[] {android.R.id.text1,
		                           android.R.id.text2})
				{
					public View getView(int position, View convertView, ViewGroup parent)
					{
						View view = super.getView(position, convertView, parent);
						TextView text1 = (TextView) view.findViewById(android.R.id.text1);
						TextView text2 = (TextView) view.findViewById(android.R.id.text2);
						text1.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
						text2.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 10);
						
						Pattern p = Pattern.compile("([a-z0-9]+)");
						Matcher m = p.matcher(text2.getText().toString());
						
						if(m.find())
						{
							String u1 = m.group(1);
							if(!u1.equals(currentUser.getUsername()))
							{
								text1.setTextColor(Color.GRAY);
								text2.setTextColor(Color.GRAY);
							}
						}
						
						return view;
					}
				};
				chatView.setAdapter(adapter);
				chatView.setSelection(adapter.getCount() - 1);
				System.out.println("Finished updating adapter");
			}
		});
	}
	
	public void sendMessage(View v)
	{
		loop = false;
		Runnable r = new Runnable(){
			@Override
			public void run()
			{
				System.out.println("MESSAGE SENDING");
				final EditText input = (EditText) findViewById(R.id.message_chat_input);
				String textInput = input.getText().toString();
				
				final ParseObject message = new ParseObject(MyMessage.TABLE_NAME);
				message.put(MyMessage.CREATOR_USERNAME, currentUser.getUsername());
				message.put(MyMessage.RECEIVER_USERNAME, otherUser.getUsername());
				message.put(MyMessage.MESSAGE, textInput);
				message.put(MyMessage.CHAT_PARENT, mostRecentChat);
				try
				{
					System.out.println("Before saving msg");
					message.save();
					System.out.println("after saving msg");
				}
				catch (ParseException e1)
				{
					e1.printStackTrace();
				}
				
				updateLatestChat();
				
				ParseRelation<ParseObject> relation = mostRecentChat.getRelation(MyMessage.TABLE_NAME);
				relation.add(message);
				
				System.out.println("CHAT SAVING");
				
				try
				{
					mostRecentChat.save();
				}
				catch (ParseException e1)
				{
					e1.printStackTrace();
				}
				System.out.println("CHAT SAVED");
				input.setText("");
			}
		};
		loop = true;
		listHandler.postAtFrontOfQueue(r);
		listHandler.post(updateRunnable);
	}
	
	// TODO: Have messages able to handle attachments
	public void attachment(View v)
	{
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.message_chat, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		else if(id == R.id.action_logout)
		{
			ParseUser.logOut();
			ParseUser.getCurrentUser();
			Intent intent = new Intent(this, LogInActivity.class);
			startActivity(intent);
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
}