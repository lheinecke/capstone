package edu.neumont.swimmer_account;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import edu.neumont.Models.Suggestions;
import edu.neumont.socialfitswimmers.LogInActivity;
import edu.neumont.socialfitswimmers.R;

public class SuggestionsActivity extends ActionBarActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_suggestion);
		
		Parse.initialize(this, "KUOeDfzwtxhQ2ux0zGVgLnxO1XGtpNCS2YFIo065", "sCD4VCKI2J6BpyElGNGH89agLXziONgVh4odYqFU");
		
//		String techniquePicked = getIntent().getStringExtra(SwimmerStrokeSelectionActivity.EXTRA_TECHNIQUE);
//		String coachPicked = getIntent().getStringExtra(SwimmersChoiceCoachSuggestionActivity.EXTRA_COACH_USERNAME);
		String techniquePicked = getIntent().getStringExtra("techniquePicked");
		String userPicked = getIntent().getStringExtra("userPicked");
		String suggestedList = "";
		
		if(techniquePicked.equals(SwimmerStrokeSelectionActivity.techniques[0])) // Butterfly
		{
			suggestedList = Suggestions.BUTTERFLY_SUGGESTIONS;
		}
		else if(techniquePicked.equals(SwimmerStrokeSelectionActivity.techniques[1])) // Backstroke
		{
			suggestedList = Suggestions.BACKSTROKE_SUGGESTIONS;
		}
		else if(techniquePicked.equals(SwimmerStrokeSelectionActivity.techniques[2])) // Breaststroke
		{
			suggestedList = Suggestions.BREASTSTROKE_SUGGESTIONS;
		}
		else if(techniquePicked.equals(SwimmerStrokeSelectionActivity.techniques[3])) // Freestyle
		{
			suggestedList = Suggestions.FREESTYLE_SUGGESTIONS;
		}
		
		ParseQuery<ParseObject> coachQuery = ParseQuery.getQuery(Suggestions.TABLE_NAME);
		coachQuery.whereEqualTo(Suggestions.USERNAME, userPicked);
		ParseObject queryResult = null;
		try {
			queryResult = coachQuery.getFirst();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		List<String> suggestions = queryResult.getList(suggestedList);
		
		if(suggestions != null)
		{
			ListAdapter techAdapt = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, suggestions);
	        ListView suggestionList = (ListView) findViewById(R.id.stroke_suggestions);
	        suggestionList.setAdapter(techAdapt);
		}
		else
		{
			Toast.makeText(this, "Sorry this user doesn't have any suggestions for this subject!", Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.coach_suggestion, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		else if(id == R.id.action_logout)
		{
			ParseUser.logOut();
			ParseUser.getCurrentUser();
			Intent intent = new Intent(this, LogInActivity.class);
			startActivity(intent);
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
}