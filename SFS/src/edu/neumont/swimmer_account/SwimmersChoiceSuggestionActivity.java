package edu.neumont.swimmer_account;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import edu.neumont.Models.UserLists;
import edu.neumont.socialfitswimmers.LogInActivity;
import edu.neumont.socialfitswimmers.R;

public class SwimmersChoiceSuggestionActivity extends ActionBarActivity {

	private ParseUser currentUser;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_swimmers_choice_coach_suggestion);
		
		Parse.initialize(this, "KUOeDfzwtxhQ2ux0zGVgLnxO1XGtpNCS2YFIo065", "sCD4VCKI2J6BpyElGNGH89agLXziONgVh4odYqFU");
		currentUser = ParseUser.getCurrentUser();
		
		ParseQuery<ParseObject> query = ParseQuery.getQuery(UserLists.TABLE_NAME);
		query.whereEqualTo(UserLists.USERNAME, currentUser.getUsername());
		
		ParseObject userObject = null;
		try {
			userObject = query.getFirst();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String listChoice = getIntent().getStringExtra("listChoice");
		List<String> listObjects = null;
		
		if(listChoice.equals("friend"))
		{
			listObjects = userObject.getList(UserLists.MY_FRIENDS_LIST);
		}
		else
		{
			listObjects = userObject.getList(UserLists.MY_COACH_LIST);
		}
		
		ListAdapter techAdapt = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listObjects);
        ListView coachSelection = (ListView) findViewById(R.id.swimmer_coach_selection);
        coachSelection.setAdapter(techAdapt);
        
        final Context con = this;
        final String techniquePicked = getIntent().getStringExtra(SwimmerStrokeSelectionActivity.EXTRA_TECHNIQUE);
        
        coachSelection.setOnItemClickListener(new AdapterView.OnItemClickListener() 
        {
        	@Override
			public void onItemClick(AdapterView<?> adapterView,	View view, int i, long l)
        	{   
        		String userPicked = adapterView.getItemAtPosition(i).toString();
        		
        		Intent intent = new Intent(con, SuggestionsActivity.class);
        		intent.putExtra("techniquePicked", techniquePicked);
        		intent.putExtra("userPicked", userPicked);
        		startActivity(intent);
        	}
		});  
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.swimmers_choice_coach_suggestion, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		else if(id == R.id.action_logout)
		{
			ParseUser.logOut();
			ParseUser.getCurrentUser();
			Intent intent = new Intent(this, LogInActivity.class);
			startActivity(intent);
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
}